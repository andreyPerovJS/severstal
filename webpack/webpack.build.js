const merge = require('webpack-merge')

const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

const baseWebpackConfig = require('../webpack.config.js')

const buildWebpackConfig = {
  mode: 'production',
  plugins: [
    new CleanWebpackPlugin(),
    new MiniCssExtractPlugin({
      filename: 'style.css',
      chunkFilename: '[id].css',
    })
  ]
}

module.exports = merge(baseWebpackConfig, buildWebpackConfig)
