const webpack = require('webpack')
const merge = require('webpack-merge')

const baseWebpackConfig = require('../webpack.config.js')

const devWebpackConfig = {
  mode: 'development',
  watch: true,
  devtool: 'inline-source-map',
  devServer: {
    historyApiFallback: true,
    contentBase: `${baseWebpackConfig.externals.paths.src}/template`,
    clientLogLevel: 'error',
    stats: 'minimal',
    overlay: true,
    port: 3000
  }
}

module.exports = merge(baseWebpackConfig, devWebpackConfig)
