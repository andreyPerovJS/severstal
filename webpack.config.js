const webpack = require('webpack')
const { resolve } = require('path')

const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

const PATHS = {
  dist: resolve(__dirname, 'dist'),
  src: resolve(__dirname, 'src'),
  webpack: resolve(__dirname, 'webpack'),
  assets: resolve(__dirname, 'src/assets')
}

const HOST = {
  staging: 'http://localhost:3000',
  production: 'http://localhost:3000'
}

const isProd = process.env.NODE_ENV === 'production'

module.exports = {
  externals: {
    paths: PATHS
  },
  entry: PATHS.src,
  output: {
    filename: 'build.[hash].js',
    path: PATHS.dist
  },
  resolve: {
    extensions: ['.js'],
    alias: {
      '@helper': resolve(__dirname, './src/helper'),
      '@assets': resolve(__dirname, './src/assets'),
    }
  },
  module: {
    rules : [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
              presets: [
                '@babel/preset-env'
              ]
          }
        }
      },
      {
        test: /\.(pug|jade)$/,
        loader: 'pug-loader'
      },
      {
        test: /\css|.s[ac]ss$/i,
        use: [
          isProd ? 
            {
              loader: MiniCssExtractPlugin.loader,
              options: {
                hmr: !isProd,
                reloadAll: true
              },
            } 
          :
          'style-loader',
          'css-loader',
          {
            loader: 'sass-loader',
            options: {
              sourceMap: true,
              sassOptions: {
                outputStyle: 'compressed',
              }
            }
          },
          {
            loader: 'postcss-loader',
            options: {
              sourceMap: isProd,
              config: {
                path: `${PATHS.webpack}/postcss.config.js`
              }
            }
          },
        ],
      },
      {
        test: /\.jpe?g$|\.gif$|\.png$|\.svg$|\.woff$|\.ico$|\.ttf$|\.wav$|\.mp3$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: `images/[name].[ext]`,
            }
          }
        ]
      },
    ]
  },
  plugins: [
    new webpack.DefinePlugin({
      NODE_ENV: JSON.stringify(process.env.NODE_ENV),
      HOST: JSON.stringify(HOST)
    }),
    new HtmlWebpackPlugin({
      hash: true,
      title: 'Your App',
      template: `${PATHS.src}/template.jade`,
      filename: `${PATHS.dist}/index.html`,
      favicon: `${PATHS.assets}/favicon.ico`
    }),
  ]
}
